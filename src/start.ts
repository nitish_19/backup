const question: any = document.getElementById("question");
const choices = Array.from(document.getElementsByClassName("choice-text"));
const progressText = document.getElementById("progressText");
const scoreText: any = document.getElementById("score");
const progressBarFull = document.getElementById("progressBarFull");
let currentQuestion: any = {};
let acceptingAnswers = false;
let score = 0;
let questionCounter = 0;
let availableQuesions = [];

interface Element {
   dataset: [],
   innerText: String
}

let questions = [
  {
    question: "Level of a node is distance from root to that node. For example, level of root is 1 and levels of left and right children of root is 2. The maximum number of nodes on level i of a binary tree is:-",
    choice1: "2^(i-1)",
    choice2: "2^i",
    choice3: "2^(i+1)",
    choice4: "log(2^i)",
    answer: 1
  },
  {
    question:
      "The maximum number of binary trees that can be formed with three unlabeled nodes is:",
    choice1: "1",
    choice2: "2",
    choice3: "5",
    choice4: "7",
    answer: 3
  },
  {
    question: "The height of a binary tree is the maximum number of edges in any root to leaf path. The maximum number of nodes in a binary tree of height h is:",
    choice1: "2^h",
    choice2: "2^(h-1)",
    choice3: "2^(h*h)",
    choice4: "2^(h+1)-1;",
    answer: 4
  },
  {
    question: "What is the worst case time complexity for search, insert and delete operations in a general Binary Search Tree?",
    choice1: "O(n) for all",
    choice2: "O(logn) for all",
    choice3: "O(n) for delete and O(logn) for insert ans search",
    choice4: "O(n) for search and O(logn)for insert and delete;",
    answer: 1
  }
];

const CORRECT_BONUS = 100;
const MAX_QUESTIONS = 4;

const startGame = () => {
  questionCounter = 0;
  score = 0;
  availableQuesions = [...questions];
  getNewQuestion();
};

const getNewQuestion = () => {
  if (availableQuesions.length === 0 || questionCounter >= MAX_QUESTIONS) {
    //localStorage.setItem("mostRecentScore", score);
    return window.location.href = 'http://localhost:1234/src/end.html'

  }
  questionCounter++;
  progressText.innerText = `Question ${questionCounter}/${MAX_QUESTIONS}`;
  //Update the progress bar
  progressBarFull.style.width = `${(questionCounter / MAX_QUESTIONS) * 100}%`;

  const questionIndex = Math.floor(Math.random() * availableQuesions.length);
  currentQuestion = availableQuesions[questionIndex];
  question.innerText = currentQuestion.question;

  choices.forEach(choice => {
    
    const number = choice.dataset["number"];
    choice.innerText = currentQuestion["choice" + number];
  });

  availableQuesions.splice(questionIndex, 1);
  acceptingAnswers = true;
};

choices.forEach(choice => {
  choice.addEventListener("click", e => {
    if (!acceptingAnswers) return;
    acceptingAnswers = false;
    const selectedChoice = e.target as any;
    const selectedAnswer = selectedChoice.dataset["number"];

    const classToApply =
      selectedAnswer == currentQuestion.answer ? "correct" : "incorrect";

    if (classToApply === "correct") {
      incrementScore(CORRECT_BONUS);
    }

    selectedChoice.parentElement.classList.add(classToApply);

    setTimeout(() => {
      selectedChoice.parentElement.classList.remove(classToApply);
      getNewQuestion();
    }, 1000);
  });
});

const incrementScore = num => {
  score += num;
  scoreText.innerText = score;
};

startGame();
